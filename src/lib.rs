#[macro_use] extern crate vst;

use vst::buffer::AudioBuffer;
use vst::plugin::{Plugin, Info};

use std::mem;
use std::collections::VecDeque;

type SamplePair = (f32, f32);

const EMPTY_SAMPLE: SamplePair = (0.0, 0.0);
const MAX_TIME_MS: f32 = 500.0;              // 500ms
const NUM_CHANNELS: i32 = 2;                 // stereo
const SAMPLE_RATE_KHZ: f32  = 96.0;          // 96kHz

// time parameter between 0.0 and 0.1 (as a fraction of MAX_TIME_MS)
fn time_to_num_samples(time: f32) -> i32 {
    let time_ms = MAX_TIME_MS * time;

    (time_ms * (NUM_CHANNELS as f32) * SAMPLE_RATE_KHZ) as i32
}

// merge two samples into a single sample by computing their average
fn stereo_to_mono(left: f32, right: f32) -> f32 {
    (left + right) / 2.0
}

struct Delay {
    buffer: VecDeque<SamplePair>,
    delay_time: f32,
    feedback: f32,
    dry_wet: f32,
}

impl Default for Delay {
    fn default() -> Delay {
        Delay::new(0.33, 0.5, 0.5)
    }
}

impl Delay {
    fn new (delay_time: f32, feedback: f32, dry_wet: f32) -> Delay {
        let num_samples = time_to_num_samples(delay_time);
        let mut buffer = VecDeque::with_capacity(num_samples as usize);

        // fill in delay buffer with empty samples
        for _ in 0..num_samples {
            buffer.push_back(EMPTY_SAMPLE);
        }

        Delay {
            buffer: buffer,
            delay_time: delay_time,
            feedback: feedback,
            dry_wet: dry_wet,
        }
    }

    // resize buffer to match new delay time
    fn resize (&mut self, time: f32) {
        let old_time = mem::replace(&mut self.delay_time, time);

        let old_size = time_to_num_samples(old_time);
        let new_size = time_to_num_samples(time);
        let diff_size = new_size - old_size;

        if diff_size > 0 {
            for _ in 0..diff_size {
                self.buffer.push_back(EMPTY_SAMPLE);
            }
        } else {
            for _ in 0..-diff_size {
                let _ = self.buffer.pop_front();
            }
        }
    }

    // mix samples according to dry/wet ratio
    fn mix (&mut self, dry_sample: f32, wet_sample: f32) -> f32 {
        ((1.0 - self.dry_wet) * dry_sample) + (self.dry_wet * wet_sample)
    }
}

impl Plugin for Delay {
    fn get_info(&self) -> Info {
        Info {
            name: "delay".to_string(),
            vendor: "panchaea".to_string(),
            unique_id: 9285,
            inputs: 2,
            outputs: 2,
            parameters: 3,
            ..Info::default()
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match index {
            0 => self.delay_time,
            1 => self.feedback,
            2 => self.dry_wet,
            _ => 0.0,
        }
    }

    fn set_parameter(&mut self, index: i32, value: f32) {
        match index {
            0 => self.resize(value.max(1.0 / MAX_TIME_MS).min(1.0)),
            1 => self.feedback = value.max(0.0).min(0.95),
            2 => self.dry_wet = value.max(0.0).min(1.0),
            _ => (),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match index {
            0 => "Delay Time".to_string(),
            1 => "Feedback".to_string(),
            2 => "Dry/Wet".to_string(),
            _ => "".to_string(),
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match index {
            0 => format!("{}", (self.delay_time * 500.0) as isize),
            1 => format!("{:.1}", self.feedback * 100.0),
            2 => format!("{:.1}", self.dry_wet * 100.0),
            _ => "".to_string(),
        }
    }

    fn get_parameter_label(&self, index: i32) -> String {
        match index {
            0 => "ms".to_string(),
            1 => "%".to_string(),
            2 => "%".to_string(),
            _ => "".to_string(),
        }
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let (inputs, mut outputs) = buffer.split();

        // split inputs to left/right channels
        let (l, r) = inputs.split_at(1);
        let stereo_in = l[0].iter().zip(r[0].iter());

        // split outputs to mutable left/right channels
        let (mut l, mut r) = outputs.split_at_mut(1);
        let stereo_out = l[0].iter_mut().zip(r[0].iter_mut());

        // for every stereo input/output sample
        for ((left_in, right_in), (left_out, right_out)) in stereo_in.zip(stereo_out) {
            let (mut left_processed, mut right_processed) = (0.0, 0.0);
            let (mut left_feedback, mut right_feedback) = (0.0, 0.0);

            // get the front of the delay line buffer and feed an attenuated
            // version back into the delay line
            if let Some((left_old, right_old)) = self.buffer.pop_front() {
                let old_mono = stereo_to_mono(left_old, right_old);
                left_processed += old_mono;
                right_processed -= old_mono;

                left_feedback += left_old * self.feedback;
                right_feedback += right_old * self.feedback;
            }

            self.buffer.push_back((*left_in + left_feedback, *right_in + right_feedback));

            // mix dry signal with wet (delay) signal
            *left_out = self.mix(*left_in, left_processed);
            *right_out = self.mix(*right_in, right_processed);
        }
    }
}

plugin_main!(Delay);
