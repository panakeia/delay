# delay
A simplistic delay VST implemented in Rust. Uses [rust-vst](https://github.com/rust-dsp/rust-vst).

Has controls for delay time (1-500ms), feedback, and dry/wet.

## To do
  - Implement independent stereo delay
  - Improve transition when changing delay time
  - Add additional processing (e.g. saturation, filters) to delay feedback loop
  - Add quantization options

## Usage
  1. Install [Rust](https://www.rust-lang.org/).
  2. Clone this repository.
  3. Run `cargo build` and copy `target/debug/delay.dll` to your VST plugins folder.
